cmake_minimum_required (VERSION 3.10.0)
project (ourwebasm)

SET(SRC	test.cpp)


set(CMAKE_EXECUTABLE_SUFFIX ".html")
add_executable(ourwebasm  ${SRC})


target_link_libraries(ourwebasm /home/apivan/webemplaylib/build/libourwebasm.a)

target_include_directories(ourwebasm
 PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}
 PUBLIC "/home/apivan/webemplaylib/" )



set_property(TARGET ourwebasm PROPERTY CXX_STANDARD 14)

#message("fff" ${CMAKE_EXE_LINKER_FLAGS})

set_target_properties(ourwebasm PROPERTIES LINK_FLAGS  "-o ourwebasm.html")

#SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS}")
#SET(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${GCC_COVERAGE_LINK_FLAGS}")

#set_target_properties(ourwebasm PROPERTIES LINK_FLAGS "-o dist/ourwebasm.js -s USE_FREETYPE=1 -s DISABLE_EXCEPTION_CATCHING=0 -s DEMANGLE_SUPPORT=1 -s SAFE_HEAP=1 --bind -s WASM=1 -O2 -s LEGACY_GL_EMULATION=0  -s GL_UNSAFE_OPTS=0 --pre-js pre-module.js --post-js post-module.js -s ASSERTIONS=1 -s GL_ASSERTIONS=1 -s INVOKE_RUN=0  -std=c++11 -s USE_WEBGL2=1 -s FULL_ES3=1 -s USE_GLFW=3 -s OFFSCREENCANVAS_SUPPORT=1 --preload-file textures --preload-file shaders --preload-file fonts")




