#include <iostream>
#include <sstream>
using namespace std;

#include "getsize.hpp"

std::string buildstring() 
{    
    std::stringstream ss;
    ss << "Size of char: " << sizeof(char) << " byte" << "\n";
    ss << "Size of long: " << sizeof(long) << " byte" << "\n";
    ss << "Size of ulong: " << sizeof(unsigned long) << " byte" << "\n";
    ss << "Size of size_t: " << sizeof(std::size_t) << " byte" << "\n";
    ss << "Size of int: " << sizeof(int) << " bytes" << "\n";
    ss << "Size of float: " << sizeof(float) << " bytes" << "\n";
    ss << "Size of double: " << sizeof(double) << " bytes" << "\n";
    return ss.str();
    
}
